#include <avr/io.h>
#include <util/delay.h>
#include <stdbool.h>
#include <avr/eeprom.h> //to store the test result
//use last EEPROM cell
#define EEADDR 0x1FF

/*========================
*   Import section
*=========================*/
#include "lib/lcd_screen.h"
#include "lib/serial.h"
#include "lib/diag_keypad.h"
#include "lib/diag_lcd.h"
#include "lib/diag_potentiometre.h"
#include "lib/diag_contactor.h"
#include "lib/diag_led_seg.h"
#include "lib/diag_buttons.h"
#include "lib/diag_prox_sensor.h"


/*=======================
*   Fault code defines
*=======================*/
#define FAULTY_LCD                  0
#define FAULTY_KEYPAD               1
#define FAULTY_POTENTIOMETER        2
#define FAULTY_PROX_SENSOR          3
#define FAULTY_LED_SEG              4
#define FAULTY_CONTACTORS           5
#define FAULTY_BUTTONS              6

//== answer codes ==
#define DIAG_REQUESTED              0
#define CONTINUE_AFTER_DIAG         0

//previous declaration
void print_test_result(uint8_t result);
uint8_t diagRequested(void);
void printString(uint8_t* str, bool isFaulty);
uint8_t countSetBits(uint8_t n);

/******************************* Main Program Code *************************/
int main(void)
{
    /*========================
    *   internal variables
    *=========================*/
    // == DIAG RESULT       ==
    uint8_t last_result =0;
    uint8_t result      =0;

    if(diagRequested() == DIAG_REQUESTED){
        /*========================
        *   launch tests
        *=========================*/
        // == LCD                       ==
        result |= (test_lcd() << FAULTY_LCD);
        // == KEYPAD                    ==
        result |= (test_keypad() << FAULTY_KEYPAD);
        // == POTENTIOMETER             ==
        result |= (test_potentiometre() << FAULTY_POTENTIOMETER);
        // == CONTACTORS                ==
        result |= (test_contactor() << FAULTY_CONTACTORS);
        // == LED AND 7 SEGMENT DISPLAY ==
        result |= (test_led_seg() << FAULTY_LED_SEG);
        // == PUSH BUTTONS ==
        result |= (test_buttons() << FAULTY_BUTTONS);
        // == PROXIMITY SENSOR  ==
        result |= (test_prox_sensor() << FAULTY_PROX_SENSOR);

        //read last result and if different, store new result in eeprom
        eeprom_busy_wait();
        last_result = eeprom_read_byte((uint8_t*)EEADDR);

         if(last_result!=result){
            //store the result in eeprom
            eeprom_busy_wait();
            eeprom_write_byte((uint8_t*)EEADDR, result);
         }

        // print test result
        print_test_result(eeprom_read_byte((uint8_t*)EEADDR));
    }

    // Start user program
    printString((uint8_t *)"PROG START :)",false);
    // Turn off the screen
    lcd_aled_port &= ~(1<<lcd_aled_bit);


    return 0;
}

void printString(uint8_t* str, bool isFaulty){
    lcd_check_BF_8();
    lcd_write_instruction_8f(lcd_Clear);
    if(isFaulty){
        lcd_write_string_8f((uint8_t*)"FAULTY");
        lcd_write_instruction_8f(lcd_SetCursor|lcd_LineTwo);
        lcd_write_string_8f(str);
        _delay_ms(DISP_WAIT);
    }else{
        lcd_write_string_8f(str);
        _delay_ms(DISP_WAIT);
    }
}

void print_test_result(uint8_t result){
    // === INTERNAL VARIABLES ===
    uint8_t answer;
    char str_error_code[9]="";

    if(result & (1<<FAULTY_LCD)){
        printString((uint8_t *)"LCD",true);
    }
    if(result & (1<<FAULTY_KEYPAD)){
        printString((uint8_t *)"KEYPAD",true);
    }
    if(result & (1<<FAULTY_POTENTIOMETER)){
        printString((uint8_t *)"POTENTIOMETER",true);
    }
    if(result & (1<<FAULTY_CONTACTORS)){
        printString((uint8_t *)"SWITCHS",true);
    }
    if(result & (1<<FAULTY_LED_SEG)){
        printString((uint8_t *)"LEDs OR SEGs",true);
    }
    if(result & (1<<FAULTY_BUTTONS)){
        printString((uint8_t *)"BUTTONS",true);
    }
    if(result & (1<<FAULTY_PROX_SENSOR)){
        printString((uint8_t *)"PROX SENSOR",true);
    }
    printString((uint8_t *)"RESULT : ",false);
    lcd_write_string_8f((uint8_t *)itoa(result,str_error_code,2));
    lcd_write_instruction_8f(lcd_SetCursor|lcd_LineTwo);
    lcd_write_string_8f((uint8_t *)"*: CONTINUE ");

    // === USER ANSWER ===
    while(answer != CONTINUE_AFTER_DIAG){
        answer = get_test_answer();
    }


}

uint8_t diagRequested(void){
    // === INTERNAL VARIABLES ===
    uint8_t answer;
    char str_nb_errors[2]="";

    //=== INIT LCD ===
    // initialize the LCD controller as determined by the defines (LCD instructions)
    lcd_init_8f();                                  // initialize the LCD display for an 8-bit interface
    lcd_aled_port |= (1<<lcd_aled_bit);
    lcd_check_BF_8();
    lcd_write_instruction_8f(lcd_Clear);

    //=== PRINT QUESTION ===
    lcd_check_BF_8();
    lcd_write_instruction_8f(lcd_SetCursor|lcd_LineOne);
    lcd_write_string_8f((uint8_t*) "START DIAG?");
    // === DISPLAY FAULT RESULT CODE AT STARTUP ===
    lcd_write_string_8f((uint8_t*) "Err:");
    lcd_write_string_8f((uint8_t*)itoa(countSetBits(eeprom_read_byte((uint8_t*)EEADDR)),str_nb_errors,10));
    lcd_write_instruction_8f(lcd_SetCursor|lcd_LineTwo);
    lcd_write_string_8f((uint8_t*) "*:yes 1:no");

    // === USER ANSWER ===
    while(1){
        answer = get_test_answer();
        if (answer!= NOP){
            _delay_ms(DISP_WAIT);
            return answer;
        }
    }
}

uint8_t countSetBits(uint8_t n)
{
    uint8_t count = 0;
    while (n) {
        count += n & 1;
        n >>= 1;
    }
    return count;
}
