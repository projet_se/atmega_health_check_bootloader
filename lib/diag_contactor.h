#ifndef DIAG_CONTACTOR_H_INCLUDED
#define DIAG_CONTACTOR_H_INCLUDED

#include <stdint.h>

void display_tab(int* tab,int nbSymb);
uint8_t test_contactor(void);

#endif // DIAG_CONTACTOR_H_INCLUDED
