#ifndef SERIAL_H_INCLUDED
#define SERIAL_H_INCLUDED

/*==========================
*  Preprocessor directives
*===========================*/

//USART Configuration (we use here Asynchronous Normal Mode)
//baudrate conf

#define BAUD 115200
#define BRC  ((F_CPU/16/BAUD)-1)//check datasheet

#define RX_BUFFER_SIZE 128

void init_serial(void);
char peekChar(int rxReadPos,int rxWritePos, char* rxBuffer);
char getChar();

#endif // SERIAL_H_INCLUDED
