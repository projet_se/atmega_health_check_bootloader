#ifndef DIAG_LED_SEG_H_INCLUDED
#define DIAG_LED_SEG_H_INCLUDED
#include <stdint.h>

uint8_t test_led_seg(void);

#endif // DIAG_LED_SEG_H_INCLUDED
