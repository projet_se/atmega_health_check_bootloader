#ifndef KEYPAD_H_INCLUDED
#define KEYPAD_H_INCLUDED

#define FAULTY_KEYPAD               1
//== fault codes ==
#define GOOD    0
#define FAULTY  1
#define NOP     2

//== button code ==
#define ERR_BTN         '1'
#define CONTINUE_BTN    '*'

//== response durations
#define DISP_WAIT   800
#define SHORT_TEMPO 100

#include <stdint.h>


uint8_t get_test_answer(void);

uint8_t test_keypad(void);


#endif // KEYPAD_H_INCLUDED
