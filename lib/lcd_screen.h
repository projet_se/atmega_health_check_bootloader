#ifndef LCD_SCREEN_H_INCLUDED
#define LCD_SCREEN_H_INCLUDED


/****************************************************************************
    LCD-AVR-8f.c  - Use an HD44780U based LCD with an Atmel ATmega processor

    Copyright (C) 2013 Donald Weiman    (weimandn@alfredstate.edu)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/****************************************************************************
         File:    LCD-AVR-8f.c
         Date:    September 16, 2013

       Target:    ATmega2560
     Compiler:    avr-gcc (AVR Studio 6)
       Author:    Donald Weiman

      Summary:    8-bit data interface, with busy flag implemented
                  Any LCD pin can be connected to any available I/O port
                  Includes a simple write string routine.
*/
/******************************* Program Notes ******************************

            This program uses an 8-bit data interface and it uses the busy
              flag to determine when the LCD controller is ready.  The LCD
              RW line (pin 5) must therefore be connected to the uP.

            The use of the busy flag does not mean that all of the software
              time delays have been eliminated.  There are still several
              required in the LCD initialization routine where the busy flag
              cannot yet be used.  These delays are have been implemented at
              least twice as long as called for in the data sheet to
              accommodate some of the out of spec displays that may show up.
              There are also some other software time delays required to
              implement timing requirements such as setup and hold times for
              the various control signals.

  ***************************************************************************

            The eight data lines as well as the three control lines may be
              implemented on any available I/O pin of any port.  These are
              the connections used for this program:

                 -----------                   ----------
                | ATmega2560|                 |   LCD    |
                |           |                 |          |
                |        PB7|---------------->|D7        |
                |        PB6|---------------->|D6        |
                |        PB5|---------------->|D5        |
                |        PB4|---------------->|D4        |
                |        PB3|---------------->|D3        |
                |        PB2|---------------->|D2        |
                |        PB1|---------------->|D1        |
                |        PB0|---------------->|D0        |
                |           |                 |          |
                |        PG2|---------------->|E         |
                |        PG1|---------------->|RW        |
                |        PG0|---------------->|RS        |
                |        PG5|---------------->|LED-A     |
                |           |          GND--->|LED-K     |
                 -----------                   ----------

  **************************************************************************/

#include <avr/io.h>
#include <avr/interrupt.h> //to be able to detect wether a msg is received


/*==========================
*  Preprocessor directives
*===========================*/

// LCD interface (should agree with the diagram above)
#define lcd_aled_port   PORTG
#define lcd_aled_bit    PORTG5
#define lcd_aled_ddr    DDRG

#define lcd_D7_port     PORTB                   // lcd D7 connection
#define lcd_D7_bit      PORTB7
#define lcd_D7_ddr      DDRB
#define lcd_D7_pin      PINB                    // busy flag

#define lcd_D6_port     PORTB                   // lcd D6 connection
#define lcd_D6_bit      PORTB6
#define lcd_D6_ddr      DDRB

#define lcd_D5_port     PORTB                   // lcd D5 connection
#define lcd_D5_bit      PORTB5
#define lcd_D5_ddr      DDRB

#define lcd_D4_port     PORTB                   // lcd D4 connection
#define lcd_D4_bit      PORTB4
#define lcd_D4_ddr      DDRB

#define lcd_D3_port     PORTB                   // lcd D3 connection
#define lcd_D3_bit      PORTB3
#define lcd_D3_ddr      DDRB

#define lcd_D2_port     PORTB                   // lcd D2 connection
#define lcd_D2_bit      PORTB2
#define lcd_D2_ddr      DDRB

#define lcd_D1_port     PORTB                   // lcd D1 connection
#define lcd_D1_bit      PORTB1
#define lcd_D1_ddr      DDRB

#define lcd_D0_port     PORTB                   // lcd D0 connection
#define lcd_D0_bit      PORTB0
#define lcd_D0_ddr      DDRB

#define lcd_E_port      PORTG                   // lcd Enable pin
#define lcd_E_bit       PORTG2
#define lcd_E_ddr       DDRG

#define lcd_RS_port     PORTG                   // lcd Register Select pin
#define lcd_RS_bit      PORTG0
#define lcd_RS_ddr      DDRG

#define lcd_RW_port     PORTG                   // lcd Read/Write pin
#define lcd_RW_bit      PORTG1
#define lcd_RW_ddr      DDRG

// LCD module information
#define lcd_LineOne     0x00                    // start of line 1
#define lcd_LineTwo     0x40                    // start of line 2
#define lcd_LineTwoMid  0x49                    // start of middle of line 2

// LCD instructions
#define lcd_Clear           0b00000001          // replace all characters with ASCII 'space'
#define lcd_Home            0b00000010          // return cursor to first position on first line
#define lcd_EntryMode       0b00000110          // shift cursor from left to right on read/write
#define lcd_DisplayOff      0b00001000          // turn display off
#define lcd_DisplayOn       0b00001100          // display on, cursor off, don't blink character
#define lcd_FunctionReset   0b00110000          // reset the LCD
#define lcd_FunctionSet8bit 0b00111000          // 8-bit data, 2-line display, 5 x 7 font
#define lcd_SetCursor       0b10000000          // set cursor position

// Function Prototypes
void lcd_write_8(uint8_t);
void lcd_write_instruction_8f(uint8_t);
void lcd_write_character_8f(uint8_t);
void lcd_write_string_8f(uint8_t *);
void lcd_init_8f(void);
void lcd_check_BF_8(void);

#endif // LCD_SCREEN_H_INCLUDED
