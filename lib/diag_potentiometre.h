#ifndef POTENTIOMETRE_H_INCLUDED
#define POTENTIOMETRE_H_INCLUDED
//Include for int stringifying
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include <stdint.h>

void adc_init();
uint16_t adc_read(uint8_t ADCchannel);
uint8_t test_potentiometre(void);

#endif
