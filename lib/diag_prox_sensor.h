#ifndef DIAG_PROX_SENSOR_H_INCLUDED
#define DIAG_PROX_SENSOR_H_INCLUDED
#include <stdint.h>

uint8_t test_prox_sensor(void);

#endif // DIAG_PROX_SENSOR_H_INCLUDED
