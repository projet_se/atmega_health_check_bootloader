#include <avr/io.h>
#include <util/delay.h>
#include "../lib/lcd_screen.h"
#include "../lib/diag_potentiometre.h"
#include "../lib/diag_keypad.h"




void adc_init()
{
    // Select Vref=AVcc
    ADMUX |= (1<<REFS0);
    //set prescaller to 128 and enable ADC
    ADCSRA |= (1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0)|(1<<ADEN);
}


uint16_t adc_read(uint8_t ADCchannel)
{
    //select ADC channel with safety mask
    ADMUX = (ADMUX & 0xF0) | (ADCchannel & 0x0F);
    //single conversion mode
    ADCSRA |= (1<<ADSC);
    // wait until ADC conversion is complete
    while( ADCSRA & (1<<ADSC) );
   return ADC;
}

uint8_t test_potentiometre(void)
{
    //=== internal variables ===
    uint8_t result;

    //=== LCD INIT ===
    // initialize the LCD controller as determined by the defines (LCD instructions)
    lcd_init_8f();                                  // initialize the LCD display for an 8-bit interface
    lcd_aled_port |= (1<<lcd_aled_bit);

    lcd_check_BF_8();
    lcd_write_instruction_8f(lcd_Clear);


    //=== ADC Init ===
    adc_init();

    //Affichage premi�re ligne info
    lcd_check_BF_8();// make sure LCD controller is ready
    lcd_write_instruction_8f(lcd_SetCursor | lcd_LineOne);
    _delay_ms(SHORT_TEMPO);
    lcd_write_string_8f((uint8_t*)"POT OK? *:y 1:n");

    //Lecture potentiom�tre
    uint8_t ADCchannel = 0; // Potentiometre branch� � l'ADC0 (PF0)
    int pot_value;

    while(1){
        pot_value = (int) adc_read(ADCchannel);
        _delay_ms(SHORT_TEMPO);

        //Affichage valeur potentiom�tre
        lcd_check_BF_8();// make sure LCD controller is ready
        lcd_write_instruction_8f(lcd_SetCursor | lcd_LineTwo);
        lcd_check_BF_8();// make sure LCD controller is ready
        char str_pot_value[4]="";

        lcd_write_string_8f((uint8_t*)itoa(pot_value/4,str_pot_value,10));
        if(pot_value/4 < 100)
        {
            lcd_write_string_8f((uint8_t*)"   ");
        }

        // V�rifier si appui sur * ou 1
        // wait for user answer

            result = get_test_answer();
            if (result!= NOP){
                _delay_ms(DISP_WAIT);
                return result;
            }



    }


}
