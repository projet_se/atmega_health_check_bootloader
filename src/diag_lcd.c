#include "../lib/diag_lcd.h"
#include "../lib/diag_keypad.h"
#include "../lib/lcd_screen.h"
#include <util/delay.h>

#define TEST_STRING "****************"

uint8_t test_lcd(void){

    //=== internal variables ===
    uint8_t result;

    //=== LCD INIT ===
    // initialize the LCD controller as determined by the defines (LCD instructions)
    lcd_init_8f();                                  // initialize the LCD display for an 8-bit interface
    lcd_aled_port |= (1<<lcd_aled_bit);

    //=== PRINT QUESTION ===

    //clear screen
    lcd_check_BF_8();
    lcd_write_instruction_8f(lcd_Clear);

    //test first line
    lcd_write_string_8f((uint8_t*)TEST_STRING);

    //test second line
    lcd_check_BF_8();
    lcd_write_instruction_8f(lcd_SetCursor|lcd_LineTwo);
    lcd_write_string_8f((uint8_t*)TEST_STRING);

    //wait two second
    _delay_ms(DISP_WAIT*2);

    //print question
    lcd_check_BF_8();
    lcd_write_instruction_8f(lcd_Clear);
    lcd_write_instruction_8f(lcd_SetCursor|lcd_LineOne);
    lcd_write_string_8f((uint8_t*) "LCD OK? *:y 1:n");

    // wait for user answer
    while(1){
        result = get_test_answer();
        if (result!= NOP){
            _delay_ms(DISP_WAIT);
            return result;
        }
    }


}

