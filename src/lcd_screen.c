#include "../lib/lcd_screen.h"
#include <util/delay.h>

/*============================== 8-bit LCD Functions ======================*/
/*
  Name:     lcd_init_8f
  Purpose:  initialize the LCD module for a 8-bit data interface
  Entry:    equates (LCD instructions) set up for the desired operation
  Exit:     no parameters
  Notes:    uses the busy flag instead of time delays when possible
*/
void lcd_init_8f(void)
{

// configure the microprocessor pins for the data lines
    lcd_D7_ddr |= (1<<lcd_D7_bit);                  // 8 data lines - output
    lcd_D6_ddr |= (1<<lcd_D6_bit);
    lcd_D5_ddr |= (1<<lcd_D5_bit);
    lcd_D4_ddr |= (1<<lcd_D4_bit);
    lcd_D3_ddr |= (1<<lcd_D3_bit);
    lcd_D2_ddr |= (1<<lcd_D2_bit);
    lcd_D1_ddr |= (1<<lcd_D1_bit);
    lcd_D0_ddr |= (1<<lcd_D0_bit);

    lcd_aled_ddr |= (1<<lcd_aled_bit);

// configure the microprocessor pins for the control lines
    lcd_E_ddr |= (1<<lcd_E_bit);                    // E line - output
    lcd_RS_ddr |= (1<<lcd_RS_bit);                  // RS line - output
    lcd_RW_ddr |= (1<<lcd_RW_bit);                  // RW line - output

// Power-up delay
    _delay_ms(100);                                 // initial 40 mSec delay

// Reset the LCD controller
    lcd_write_instruction_8f(lcd_FunctionReset);    // first part of reset sequence
    _delay_ms(10);                                  // 4.1 mS delay (min)

    lcd_write_instruction_8f(lcd_FunctionReset);    // second part of reset sequence
    _delay_us(200);                                 // 100uS delay (min)

    lcd_write_instruction_8f(lcd_FunctionReset);    // third part of reset sequence
    _delay_us(200);                                 // this delay is omitted in the data sheet

// Function Set instruction
    lcd_write_instruction_8f(lcd_FunctionSet8bit);  // set mode, lines, and font
// --> from this point on the busy flag is available <--

// The next three instructions are specified in the data sheet as part of the initialization routine,
//  so it is a good idea (but probably not necessary) to do them just as specified and then redo them
//  later if the application requires a different configuration.

// Display On/Off Control instruction
    lcd_check_BF_8();                               // make sure LCD controller is ready
    lcd_write_instruction_8f(lcd_DisplayOff);       // turn display OFF

// Clear Display instruction
    lcd_check_BF_8();                               // make sure LCD controller is ready
    lcd_write_instruction_8f(lcd_Clear);            // clear display RAM

// ; Entry Mode Set instruction
    lcd_check_BF_8();                               // make sure LCD controller is ready
    lcd_write_instruction_8f(lcd_EntryMode);        // set desired shift characteristics

// This is the end of the LCD controller initialization as specified in the data sheet, but the display
//  has been left in the OFF condition.  This is a good time to turn the display back ON.

// Display On/Off Control instruction
    lcd_check_BF_8();                               // make sure LCD controller is ready
    lcd_write_instruction_8f(lcd_DisplayOn);        // turn the display ON
}

/*...........................................................................
  Name:     lcd_write_string_8f
; Purpose:  display a string of characters on the LCD
  Entry:    (theString) is the string to be displayed
  Exit:     no parameters
  Notes:    uses the busy flag instead of time delays
*/
void lcd_write_string_8f(uint8_t theString[])
{
    volatile int i = 0;                             // character counter*/
    while (theString[i] != 0)
    {
        lcd_check_BF_8();                           // make sure LCD controller is ready
        lcd_write_character_8f(theString[i]);
        i++;
    }
}

/*...........................................................................
  Name:     lcd_write_character_8f
  Purpose:  send a byte of information to the LCD data register
  Entry:    (theData) is the information to be sent to the data register
  Exit:     no parameters
  Notes:    configures RW (busy flag is implemented)
*/
void lcd_write_character_8f(uint8_t theData)
{
    lcd_RW_port &= ~(1<<lcd_RW_bit);                // write to LCD module (RW low)
    lcd_RS_port |= (1<<lcd_RS_bit);                 // select the Data Register (RS high)
    lcd_E_port &= ~(1<<lcd_E_bit);                  // make sure E is initially low
    lcd_write_8(theData);                           // write the data
}

/*...........................................................................
  Name:     lcd_write_instruction_8f
  Purpose:  send a byte of information to the LCD instruction register
  Entry:    (theInstruction) is the information to be sent to the instruction register
  Exit:     no parameters
  Notes:    configures RW (busy flag is implemented)
*/
void lcd_write_instruction_8f(uint8_t theInstruction)
{
    lcd_RW_port &= ~(1<<lcd_RW_bit);                // write to LCD module (RW low)
    lcd_RS_port &= ~(1<<lcd_RS_bit);                // select the Instruction Register (RS low)
    lcd_E_port &= ~(1<<lcd_E_bit);                  // make sure E is initially low
    lcd_write_8(theInstruction);                    // write the instruction
}


/*...........................................................................
  Name:     lcd_write_8
  Purpose:  send a byte of information to the LCD module
  Entry:    (theByte) is the information to be sent to the desired LCD register
            RS is configured for the desired LCD register
            E is low
            RW is low
  Exit:     no parameters
  Notes:    use either time delays or the busy flag
*/
void lcd_write_8(uint8_t theByte)
{
    lcd_D7_port &= ~(1<<lcd_D7_bit);                        // assume that data is '0'
    if (theByte & 1<<7) lcd_D7_port |= (1<<lcd_D7_bit);     // make data = '1' if necessary

    lcd_D6_port &= ~(1<<lcd_D6_bit);                        // repeat for each data bit
    if (theByte & 1<<6) lcd_D6_port |= (1<<lcd_D6_bit);

    lcd_D5_port &= ~(1<<lcd_D5_bit);
    if (theByte & 1<<5) lcd_D5_port |= (1<<lcd_D5_bit);

    lcd_D4_port &= ~(1<<lcd_D4_bit);
    if (theByte & 1<<4) lcd_D4_port |= (1<<lcd_D4_bit);

    lcd_D3_port &= ~(1<<lcd_D3_bit);
    if (theByte & 1<<3) lcd_D3_port |= (1<<lcd_D3_bit);

    lcd_D2_port &= ~(1<<lcd_D2_bit);
    if (theByte & 1<<2) lcd_D2_port |= (1<<lcd_D2_bit);

    lcd_D1_port &= ~(1<<lcd_D1_bit);
    if (theByte & 1<<1) lcd_D1_port |= (1<<lcd_D1_bit);

    lcd_D0_port &= ~(1<<lcd_D0_bit);
    if (theByte & 1<<0) lcd_D0_port |= (1<<lcd_D0_bit);

// write the data
                                                    // 'Address set-up time' (40 nS)
    lcd_E_port |= (1<<lcd_E_bit);                   // Enable pin high
    _delay_us(1);                                   // implement 'Data set-up time' (80 nS) and 'Enable pulse width' (230 nS)
    lcd_E_port &= ~(1<<lcd_E_bit);                  // Enable pin low
    _delay_us(1);                                   // implement 'Data hold time' (10 nS) and 'Enable cycle time' (500 nS)
}

/*...........................................................................
  Name:     lcd_check_BF_8
  Purpose:  check busy flag, wait until LCD is ready
  Entry:    no parameters
  Exit:     no parameters
  Notes:    main program will hang if LCD module is defective or missing
            data is read while 'E' is high
*/
void lcd_check_BF_8(void)
{
    uint8_t busy_flag_copy;                         // busy flag 'mirror'

    lcd_D7_ddr &= ~(1<<lcd_D7_bit);                 // set D7 data direction to input
    lcd_RS_port &= ~(1<<lcd_RS_bit);                // select the Instruction Register (RS low)
    lcd_RW_port |= (1<<lcd_RW_bit);                 // read from LCD module (RW high)


    do
    {
        busy_flag_copy = 0;                         // initialize busy flag 'mirror'
        lcd_E_port |= (1<<lcd_E_bit);               // Enable pin high
        _delay_us(1);                               // implement 'Delay data time' (160 nS) and 'Enable pulse width' (230 nS)

        busy_flag_copy |= (lcd_D7_pin & (1<<lcd_D7_bit));  // get actual busy flag status

        lcd_E_port &= ~(1<<lcd_E_bit);              // Enable pin low
        _delay_us(1);                               // implement 'Address hold time' (10 nS), 'Data hold time' (10 nS), and 'Enable cycle time' (500 nS )

    } while (busy_flag_copy);                       // check again if busy flag was high

// arrive here if busy flag is clear -  clean up and return
    lcd_RW_port &= ~(1<<lcd_RW_bit);                // write to LCD module (RW low)
    lcd_D7_ddr |= (1<<lcd_D7_bit);                  // reset D7 data direction to output
}


