#include "../lib/diag_contactor.h"
#include "../lib/diag_keypad.h"
#include "../lib/lcd_screen.h"
#include <util/delay.h>

#define NB_CONTACTORS   4

void display_tab(int* tab,int nbSymb){
    int i;
    for(i = nbSymb-1; i>=0;i--){
        if(tab[i]==1){
            lcd_check_BF_8();
            lcd_write_string_8f((uint8_t*)"(X)");
        }else{
            lcd_check_BF_8();
            lcd_write_string_8f((uint8_t*)"( )");
        }
    }
    lcd_write_instruction_8f(lcd_SetCursor | lcd_LineTwo);

}

uint8_t test_contactor(void){
        //=== internal variables ===
        int tab[NB_CONTACTORS]={0};
        uint8_t result;

        //=== LCD and DDRC and PORTC INIT ===
        DDRC    = 0xF0; // Init direction register for contactors as inputs (PC0-PC3)
        PORTC   =  0x10;// Init PC4 as UP
        // initialize the LCD controller as determined by the defines (LCD instructions)
        lcd_init_8f();                                  // initialize the LCD display for an 8-bit interface
        lcd_aled_port |= (1<<lcd_aled_bit);
        lcd_check_BF_8();
        lcd_write_instruction_8f(lcd_Clear);

        //=== PRINT QUESTION ===
        lcd_check_BF_8();
        lcd_write_instruction_8f(lcd_SetCursor|lcd_LineOne);
        lcd_write_string_8f((uint8_t*) "SWITCHS? *:y 1:n");
        lcd_write_instruction_8f(lcd_SetCursor|lcd_LineTwo);

        //=== START TEST ===
        while(1){

            if((PINC & (1<<PINC3)) == (1<<PINC3)){
                tab[3]=1;
            }
            else {
                tab[3]=0;
            }
            if((PINC & (1<<PINC2)) == (1<<PINC2)){
                tab[2]=1;
            }
            else {
                tab[2]=0;
            }
            if((PINC & (1<<PINC1)) == (1<<PINC1)){
                tab[1]=1;
            }
            else {
                tab[1]=0;
            }
            if((PINC & (1<<PINC0)) == (1<<PINC0)){
                tab[0]=1;
            }
            else {
                tab[0]=0;
            }
            display_tab(tab,NB_CONTACTORS);

            // check for user answer
            result = get_test_answer();
            if (result!= NOP){
                _delay_ms(DISP_WAIT);
                return result;
            }
        }
}
