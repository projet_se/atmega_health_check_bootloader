#include "../lib/diag_prox_sensor.h"
#include "../lib/diag_potentiometre.h" // For common adc init and read function
#include "../lib/diag_keypad.h"
#include "../lib/lcd_screen.h"
#include <util/delay.h>

#define RIGHT_PROX_SENSOR_ADC   1
#define LEFT_PROX_SENSOR_ADC    2
uint8_t test_prox_sensor(void){
    //=== internal variables ===
    uint8_t result;
    uint16_t left_sens_val=0;
    uint16_t right_sens_val=0;
    char str_right_value[4]="";
    char str_left_value[4]="";

    //=== LCD and PROX SNESOR INIT ===
    // init ADC
    adc_init();
    // initialize the LCD controller as determined by the defines (LCD instructions)
    lcd_init_8f();                                  // initialize the LCD display for an 8-bit interface
    lcd_aled_port |= (1<<lcd_aled_bit);
    lcd_check_BF_8();
    lcd_write_instruction_8f(lcd_Clear);

    //=== PRINT QUESTION ===
    lcd_check_BF_8();
    lcd_write_instruction_8f(lcd_SetCursor|lcd_LineOne);
    lcd_write_string_8f((uint8_t*) "PROXI S? *:y 1:n");

    //=== START TEST ===
     while(1){

            // read left sensor
            left_sens_val=adc_read(LEFT_PROX_SENSOR_ADC);
             _delay_ms(SHORT_TEMPO);

            // read right sensor
            right_sens_val=adc_read(RIGHT_PROX_SENSOR_ADC);
            _delay_ms(SHORT_TEMPO);

            // print left and right values
            lcd_write_instruction_8f(lcd_SetCursor|lcd_LineTwo);
            lcd_write_string_8f((uint8_t*)"L : ");
            lcd_write_string_8f((uint8_t*)itoa(left_sens_val/4,str_left_value,10));
            if(left_sens_val/4 < 100)
            {
                lcd_write_string_8f((uint8_t*)"   ");
            }

            lcd_write_instruction_8f(lcd_SetCursor|lcd_LineTwoMid);
            lcd_write_string_8f((uint8_t*)"R : ");
            lcd_write_string_8f((uint8_t*)itoa(right_sens_val/4,str_right_value,10));
            if(right_sens_val/4 < 100)
            {
                lcd_write_string_8f((uint8_t*)"   ");
            }


            // check for user answer
            result = get_test_answer();
            if (result!= NOP){
                _delay_ms(DISP_WAIT);
                return result;
            }

     }

}
