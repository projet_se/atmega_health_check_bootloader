#include <avr/io.h>
#include "../lib/diag_buttons.h"
#include "../lib/lcd_screen.h"
#include "../lib/diag_keypad.h"
#include "../lib/diag_contactor.h" //for common display function display_tab(int* tab,int nbSymb)
#include <util/delay.h>

#define NB_BUTTONS   2  //White and Green

uint8_t test_buttons(void){
    //=== internal variables ===
    uint8_t result;
    int tab[NB_BUTTONS]={0};

    //=== LCD, WHITE AND GREEN BTN INIT ===
    DDRD = 0x00; // initialise le port D comme �tant des entr�s ( tout est mis en entr� ici mais on utilise que le pin 1 et 0 )
    // initialize the LCD controller as determined by the defines (LCD instructions)
    lcd_init_8f();                                  // initialize the LCD display for an 8-bit interface
    lcd_aled_port |= (1<<lcd_aled_bit);

    //=== PRINT QUESTION ===
    lcd_check_BF_8();
    lcd_write_instruction_8f(lcd_SetCursor|lcd_LineOne);
    lcd_write_string_8f((uint8_t*) "BTNS OK? *:y 1:n");
    lcd_write_instruction_8f(lcd_SetCursor|lcd_LineTwo);

    //=== START TEST ===
    while(1)
    {
        if((PIND & (1<<PIND1)) != (1<<PIND1)) // Si on appui sur le bouton INT 1
        {
            tab[1]=1;
        }else {
            tab[1]=0;
        }

         if((PIND & (1<<PIND0)) != (1<<PIND0)) // Si on appui sur le bouton INT 0
        {
            tab[0]=1;
        }else {
            tab[0]=0;
        }

        // === DISPLAY CURRENT VALUE ===
        display_tab(tab,NB_BUTTONS);

        // check for user answer
        result = get_test_answer();
        if (result!= NOP){
            _delay_ms(DISP_WAIT);
            return result;
        }
    }
}
