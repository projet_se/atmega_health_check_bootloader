#include <avr/io.h>
#include "../lib/lcd_screen.h"
#include "../lib/diag_keypad.h"
#include "../lib/diag_led_seg.h"
#include <util/delay.h>

uint8_t test_led_seg(void){
    //=== internal variables ===
    uint8_t result;

    //=== LCD, LED AND 7SEG INIT ===
    // initialize the LCD controller as determined by the defines (LCD instructions)
    lcd_init_8f();                                  // initialize the LCD display for an 8-bit interface
    lcd_aled_port |= (1<<lcd_aled_bit);
    DDRA = 0xFF;    // Every pins of PORTA are OUTPUTS
    DDRC = 0xF0;    // (PC4-PC7) are OUTPUTS

    //=== PRINT QUESTION ===
    lcd_check_BF_8();
    lcd_write_instruction_8f(lcd_SetCursor|lcd_LineOne);
    lcd_write_string_8f((uint8_t*) "LED/7 SEGs OK? ");
    lcd_write_instruction_8f(lcd_SetCursor|lcd_LineTwo);
    lcd_write_string_8f((uint8_t*) "*:y 1:n ");

    //=== TURN LED AND 7 SEGMENTS DISPLAYS ON ===
    PORTC=0xF0;
    PORTA=0xFF;
    while(1){
        // Wait for user answer
        result = get_test_answer();
        if (result!= NOP){
            //turn of LEDS and SEGMENTS
            PORTC=0x00;
            PORTA=0x00;
            _delay_ms(DISP_WAIT);
            return result;
        }
    }

    // === FAULTY LED OR 7 SEGMENT DISPLAY ===
    return FAULTY;

}
