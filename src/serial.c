#include "../lib/serial.h"
#include <avr/io.h>

void init_serial(void){
//configure registers UBRR0H and UBRR0L to set the baudrate
    UBRR0H = (BRC >> 8); //8 last bits of BRC in the highest part of UBRR
    UBRR0L = BRC;//remaining bits in lower part

//serial receiver configuration
    UCSR0B = (1<<RXEN0) | (1<<RXCIE0);//status register that enable RX and fire an interrupt when data is completely received
    UCSR0C = (1<<UCSZ00) |(1<<UCSZ01); //Configure 8 bits serial messages
    DDRE = (1<<PORTE0);//USB Rx
}

//========serial functions===========
char peekChar(int rxReadPos,int rxWritePos, char* rxBuffer){//function to know what is the next char to be red
    char ret = '\0';

    if(rxReadPos != rxWritePos){
        ret = rxBuffer[rxReadPos];

    }
    return ret;
}
/*
char getChar(){
    char ret = '\0'; //initialise reten char to null charcter '\0'

    if(rxReadPos != rxWritePos){
      ret = rxBuffer[rxReadPos];

      rxReadPos++; //we continue to read

      if(rxReadPos >= RX_BUFFER_SIZE){
        rxReadPos = 0;
      }
    }
    return ret;
}

*/
