/*
*   Work inspired by https://embedds.com/interfacing-matrix-keyboard-with-avr/
*/

#include <avr/io.h>
#include "../lib/lcd_screen.h"
#include "../lib/diag_keypad.h"
#include <util/delay.h>



const char caracPad[4][3]=
    {
        {'1','2','3'},
        {'4','5','6'},
        {'7','8','9'},
        {'*','0','#'}
    };

uint8_t get_test_answer(void){
    int indexs[2]={3,1};
    int colNum=0;
    int rowNum=0;
    int i=0;
    int j=0;
    // Part 1 : On allume PC7, PC6, PC5
        PORTC |= 0xE0;
        for(i=7;i>=5;i--){
            // Part 2 : On parcourt chaque collone (PC7 � PC5)
            DDRC &=~(0xEF); // On clear le registre de direction
            DDRC |= (1<<i); // On met en sortie PCi
            //Pour chaque collone on allume et on v�rifie si PC0 ou PC1 ou PC2 ou PC3
            for(j=3;j>=0;j--){
                colNum=(i-7)*-1;
                rowNum=(j-3)*-1;
                if((PINC & (1 << j)) == (1 << j)){
                    indexs[0]=rowNum;
                    indexs[1]=colNum;
                }
            }
        }
        if(caracPad[indexs[0]][indexs[1]]==ERR_BTN){
            // === FAULTY COMPONENT ===
            return FAULTY;
        }else{
            if(caracPad[indexs[0]][indexs[1]]==CONTINUE_BTN){
                // === GOOD COMPONENT ===
                return GOOD;
            }else{
                // === NO OPERATION ===
                return NOP;
            }
        }
}

uint8_t test_keypad(void){

    //=== internal variables ===
    int indexs[2]={3,1};
    uint8_t result;
    int colNum=0;
    int rowNum=0;
    int i=0;
    int j=0;

    //=== LCD INIT ===
    // initialize the LCD controller as determined by the defines (LCD instructions)
    lcd_init_8f();                                  // initialize the LCD display for an 8-bit interface
    lcd_aled_port |= (1<<lcd_aled_bit);

    //=== PRINT QUESTION ===
    lcd_check_BF_8();
    lcd_write_instruction_8f(lcd_SetCursor|lcd_LineOne);
    lcd_write_string_8f((uint8_t*) "KEYS OK? *:y 1:n");
    lcd_write_instruction_8f(lcd_SetCursor|lcd_LineTwo);

    //=== START TEST ===
    while(1){
        // Part 1 : On allume PC7, PC6, PC5
        PORTC |= 0xE0;
        for(i=7;i>=5;i--){
            // Part 2 : On parcourt chaque collone (PC7 � PC5)
            DDRC &=~(0xEF); // On clear le registre de direction
            DDRC |= (1<<i); // On met en sortie PCi
            //Pour chaque collone on allume et on v�rifie si PC0 ou PC1 ou PC2 ou PC3
            for(j=3;j>=0;j--){
                colNum=(i-7)*-1;
                rowNum=(j-3)*-1;
                if((PINC & (1 << j)) == (1 << j)){
                    indexs[0]=rowNum;
                    indexs[1]=colNum;
                }
            }
        }

        // === DISPLAY CURRENT VALUE ===
        lcd_check_BF_8();
        lcd_write_instruction_8f(lcd_SetCursor|lcd_LineTwo);
        lcd_write_character_8f(caracPad[indexs[0]][indexs[1]]);
        _delay_ms(SHORT_TEMPO);

        result = get_test_answer();
        if (result!= NOP){
            _delay_ms(DISP_WAIT);
            return result;
        }
    }
    // === FAULTY KEYPAD ===
    return FAULTY;
}


