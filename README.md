# ATMEGA Health Check

Ce programme écrit en C permet à un ATMEGA2560 de proposer un ensemble de tests permettant de vérifier le bon fonctionnement de différents périphériques avant l’exécution d'un programme écrit par l'utilisateur.  

## Contenu du répertoire

`doc` est un répertoire contenant de la documentation relative au projet C

`lib` est un répertoire contenant tous les fichiers d'entête (.h) du projet

`src` est un répertoire contenant  tous les fichiers sources (.c) du projet

`.gitignore` est le fichier qui permet d'exclure du dépôt GitLab certains fichiers autogénérés lors des compilations 

`fuse.c` fichier autogénéré par CodeBlocks, ne nécessite pas d'être explicitement modifié 

`main.c` point d'entrée du programme, appelle les fonctions de diagnostic des différents fichiers du répertoire `src`

` health_check_code_blocks_proj.cbp` projet Code::Blocks

` health_check_code_blocks_proj.layout` fichier généré par Code::Blocks, ne nécessite pas d'être explicitement modifié 

## Pour commencer

*Ce programme a été édité et compilé sur Windows. Nous n'assurons pas la possibilité de l'éditer et de le compiler sur d'autres systèmes avec les outils suivants.*

**Afin d'éditer ces codes vous avez besoin d'installer les outils suivants :**

- [Code::Blocks 17.12](http://www.codeblocks.org/) - IDE C
- [WinAVR](https://sourceforge.net/projects/winavr/) - Contient la chaine de compilation pour ATMEGA

- Un microcontrôleur ATMEGA2560 pouvant être relié via USB à un PC

### Installation

Pour éditer le code C, vous pouvez suivres les étapes suivantes :

1 - Ouvrir [Code::Blocks 17.12](http://www.codeblocks.org/)    

2 - Cliquer sur `Fichier`-> `Ouvrir`

3 - Sélectionner le fichier ` health_check_code_blocks_proj.cbp` 

## Exécution de tests unitaires 

[PAS DE TEST UNITAIRE IMPLEMENTÉ POUR L'INSTANT]

### Cas d'usage testés

1 - Diagnostic complet de la maquette de développement

Voir vidéo : [ICI](ttps://youtu.be/G8ILYSE78Ks)

## IDE

* [Code::Blocks 17.12](http://www.codeblocks.org/) - IDE C

## Auteurs

* **Suzanne DUCROT** - Étudiante Systèmes Embarqués - Polytech LILLE
* **Laurie MALARET** - Étudiante Systèmes Embarqués - Polytech LILLE
* **Axel DEFEZ** - Étudiant Systèmes Embarqués - Polytech LILLE
* **Aviran TETIA** - Étudiant Systèmes Embarqués - Polytech LILLE

Encadrants :

* **Blaise CONRARD** - Enseignant - Polytech LILLE
* **Anne-Lise GEHIN** - Enseignant - Polytech LILLE

## License

Tous droits réservés aux encadrants du projet : Blaise CONRARD et Anne-Lise GEHIN

## Sources 

- Communication série :

  [Vidéo/Tutoriel - Michael Shoeffer](https://mschoeffler.com/2017/12/29/tutorial-serial-connection-between-java-application-and-arduino-uno/)

  [Vidéo/Tutoriel - humanHardDrive](https://www.youtube.com/watch?v=WIetgFpGzfQ&feature=youtu.be)

- Gestion écran LCD :
   [Travaux de Donald Weiman](http://web.alfredstate.edu/faculty/weimandn/programming/lcd/ATmega328/LCD_code_gcc_8f.html)

  